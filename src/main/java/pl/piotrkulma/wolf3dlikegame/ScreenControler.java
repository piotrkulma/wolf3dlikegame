package pl.piotrkulma.wolf3dlikegame;

import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import org.apache.log4j.Logger;

public class ScreenControler {
    private final static Logger LOG = Logger.getLogger(ScreenControler.class);

    private GraphicsContext graphicsContext;

    @FXML
    private Canvas screenCanvas;

    @FXML
    protected void initialize() throws Exception {

        graphicsContext = screenCanvas.getGraphicsContext2D();
        screenCanvas.setFocusTraversable(true);
        LOG.info("OK");
        graphicsContext.fillText("OK", 200, 200);
    }
}
